package com.jonlandrum.selfbalancingtree;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.jonlandrum.selfbalancingtree.avltree.AVLTreeTest;
import com.jonlandrum.selfbalancingtree.redblacktree.RedBlackTreeTest;
import com.jonlandrum.selfbalancingtree.splaytree.SplayTreeTest;

@RunWith(Suite.class)
@SuiteClasses({
    NodeTest.class,
    BinarySearchTreeTest.class,
    SelfBalancingTreeTest.class,
    AVLTreeTest.class,
    RedBlackTreeTest.class,
    SplayTreeTest.class
})
public class AllTests {}