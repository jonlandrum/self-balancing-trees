package com.jonlandrum.selfbalancingtree;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SelfBalancingTreeTest {
    private SelfBalancingTree<String> tree;
    
    @Before
    public void init() {
        tree = new SelfBalancingTree<String>();
    }
    
    @After
    public void destroy() {
        tree = null;
    }
    
    @Test
    public void testConstructorNull() {
        assertThat(tree, instanceOf(SelfBalancingTree.class));
    }
    
    @Test
    public void testConstructorString() {
        tree = new SelfBalancingTree<String>("Root");
        assertThat(tree, instanceOf(SelfBalancingTree.class));
    }
    
    @Test
    public void testConstructorNode() {
        Node<String> root = new Node<String>("Root");
        tree = new SelfBalancingTree<String>(root);
        assertThat(tree, instanceOf(SelfBalancingTree.class));
    }
}
